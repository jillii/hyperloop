$(function(){
  $dot_container = $('.slick-dots-container');

  $('.slides').slick({
	  dots: true,
	  arrows: false,
	  speed: 300,
    infinite: true,
	  slidesToShow: 1,
	  draggable: true,
    adaptiveHeight: false,
    useTransform: false,
	  appendDots: $dot_container
  });
  // initialize slick dots
  $('.slick-dots').ready(function(){
  	$nav = $(this);
  	// add active class to first dot
  	$nav.find('li:first-child').addClass('slick-active');
  	// and add slide title to each hyperloop nav dot
  	$nav.find('li').each(function(){
  		$dot = $(this),
  		$index = $dot.index() + 1,
  		$slide_title = $('.slick-slide').eq($index).find('p').first().text();
  		if (!$(this).parent().parent().hasClass("mobile")) {
        $dot.prepend("<div class='line'></div>");
        $dot.attr("data-value", $slide_title);
        $dot.attr("title", $slide_title);
      }
  	});
  });
  // make sure all (both) active dots are highlighted
  $dot_container.on('click', '.slick-dots li', function(e){
		$index = $(e.currentTarget).index();
		$('#hyperloop').attr('data-value', $index);
		$dot_container.find('li:nth-child(' + ($index + 1) + ')').addClass('slick-active');
  });

  // handle active dot during touch navigation
  $('.slick-track').ready(function(){
    $(this).on('afterChange', function(event, slick, currentSlide){
        $('#hyperloop').attr('data-value', currentSlide);
        // add active class to appropriate pink dot nav item
        $('.slick-dots').each(function(){
          $(this).find('li').eq(currentSlide).addClass("slick-active"); 
        });
    });
  });

  // set height of all slides to shortest slide
  $('.slick-track').ready(function(){
    set_heights();
    // add up and down arrows to each slide
    $slides = $(this).find('.slick-slide');
    $slides.each(function(){
      $slide = $(this);
      $nav = "<div class='nav'><div id='up'>&#171;</div><div id='down'>&#187;</div></div>";
      $slide.append($nav);
    });
  });
  $(window).resize(function(){
    set_heights();
  });
  function set_heights() {
    $min = 10000,
    $slides = $('.slick-slide');
    $slides.each(function() {
      $height = $(this).height();
      if ($height < $min) {
        $min = $height;
      }
    });
    $slides.height($min);
  };
});
// send height to iframe parent
// Don't fire on height resizing
$h = 0;

$(function() {
  $h = $( window ).height();
  setTimeout(function(){ setHeight(); }, 1000);
});
$( window ).resize( function(){
  if( $h != $( window ).height() ){
    
    $h = $( window ).height();
  } else {
        setHeight();  
  }  
});
function setHeight() {
  parent.postMessage({ if_height: $('body').outerHeight( true ) }, 'https://www.symphonyhq.com/' );
};