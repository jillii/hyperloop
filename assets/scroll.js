$('.slides').on('click', '.nav', function(e){
	$dir = $(e.target).attr("id");
	$target = $(e.currentTarget).parent().find('.text--small');

	// $scroll = 
	if ($dir == "down") {
		$target[0].scrollBy({
			top: 44,
			left: 0,
			behavior: 'smooth'
		});
	} else { // direction is left
		$target[0].scrollBy({
			top: -44,
			left: 0,
			behavior: 'smooth'
		});
	}
});