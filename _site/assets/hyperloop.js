$(function(){
  $dot_container = $('.slick-dots-container');

  $('.slides').slick({
	  dots: true,
	  arrows: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  draggable: true,
	  appendDots: $dot_container
  });
  // add active class to first dot
  $('.slick-dots').ready(function(){
  	$(this).find('li:first-child').addClass('slick-active');
  });
  // make sure all (both) active dots are highlighted
  $dot_container.on('click', '.slick-dots li', function(e){
		$index = $(e.currentTarget).index();
		$('#hyperloop').attr('data-value', $index);
		$dot_container.find('li:nth-child(' + ($index + 1) + ')').addClass('slick-active');
  });

});